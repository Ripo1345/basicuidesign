import 'package:flutter/material.dart';
import './FirstPage.dart' as first;
import './SecondPage.dart' as second;
import './ThirdPage.dart' as third;
import './FourthPage.dart'as fourth;

void main() {
  runApp(new MaterialApp(
      home: new MyTabs()
  ));
}

class MyTabs extends StatefulWidget {
  @override
  MyTabsState createState() => new MyTabsState();
}

class MyTabsState extends State<MyTabs> with SingleTickerProviderStateMixin {


  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    var headerText = new Text("News",style: new TextStyle(fontSize: 25.0,color:Colors.deepPurple ));
    var header = new DrawerHeader(child: headerText,);
    return new Scaffold(
        backgroundColor: Colors.lightBlueAccent,
        drawer: new Drawer(

        child: new ListView(
            children: <Widget>[
              new Padding(
                padding: new EdgeInsets.only(top: 50.0),
              ),
              new DrawerHeader(child: header,
              ),

              new ClipRect(
                  child: new Column(
                      children: <Widget>[
                        new ListTile(
                          title: new Text('Add Account',style: new TextStyle(fontSize: 20.0)),
                          onTap: () => _loadView('E', context),
                        ),

                        new ListTile(
                          title: new Text('Share With Friend',style: new TextStyle(fontSize: 20.0)
                            ),
                          onTap: () => _loadView('A', context),
                        ),
                        new ListTile(
                          title: new Text('Notifications',style: new TextStyle(fontSize: 20.0)),
                          onTap: () => _loadView('B', context),
                        ),
                        new ListTile(
                          title: new Text('Settings',style: new TextStyle(fontSize: 20.0)),
                          onTap: () => _loadView('C', context),
                        ),
                        new ListTile(
                          title: new Text('Help & Feedback',style: new TextStyle(fontSize: 20.0)),
                          onTap: () => _loadView('D', context),
                        ),

                      ]
                  )
              ),
            ]
        )
    ),

        appBar: new AppBar(
            title: new Text("News"),
            backgroundColor: Colors.deepOrange,
            bottom: new TabBar(
                controller: controller,
                tabs: <Tab>[
                  new Tab(icon: new Icon(Icons.dashboard)),
                  new Tab(icon: new Icon(Icons.people)),
                  new Tab(icon: new Icon(Icons.alarm_on)),
                  new Tab(icon: new Icon(Icons.voice_chat)),
                ]
            )
        ),
        bottomNavigationBar: new Material(
            color: Colors.deepOrange,
            child: new TabBar(
                controller: controller,
                tabs: <Tab>[
                  new Tab(icon: new Icon(Icons.dashboard)),
                  new Tab(icon: new Icon(Icons.people)),
                  new Tab(icon: new Icon(Icons.alarm_on)),
                  new Tab(icon: new Icon(Icons.voice_chat)),
                ]
            )
        ),
        body: new TabBarView(
            controller: controller,
            children: <Widget>[
              new first.MyTextInput(),
              new second.Secand(),
              new third.Third(),
              new fourth.Fourth ()
            ]
        )
    );

  }
  _loadView(String view, BuildContext context) {
    Navigator.of(context).pop();
    setState(() {
      if (view == 'A') {
        var viewName = 'A';
      } else if (view == 'B') {
        var viewName = 'B';
      } else if (view == 'C') {
        var viewName = 'C';
      } else if (view == 'D') {
        var viewName = 'D';
      }
      else if (view == 'E') {
        var viewName = 'E';
      }
    });
  }
}