import 'package:flutter/material.dart';

class Secand extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
     // appBar: new AppBar(title: new Text("Sports News")),
      backgroundColor: Colors.lightBlueAccent,
        body: new Container(
          padding: new EdgeInsets.all(10.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new MyCard(
                  title: new Text("Messi",style: new TextStyle(fontSize: 20.0)),
                  icon: new Icon(Icons.people,size: 50.0,color:Colors.deepPurple)
              ),
              new MyCard(  title: new Text("Aguera",style: new TextStyle(fontSize: 20.0)),
                  icon: new Icon(Icons.people,size: 50.0,color:Colors.deepPurple)),
              new MyCard(  title: new Text("Ronaldo",style: new TextStyle(fontSize: 20.0)),
                  icon: new Icon(Icons.people,size: 50.0,color:Colors.deepPurple)),
              new MyCard(
                  title: new Text("Shakib",style: new TextStyle(fontSize: 20.0)),
                  icon: new Icon(Icons.people,size: 50.0,color:Colors.deepPurple)
              ),
              new MyCard(  title: new Text("Tamim",style: new TextStyle(fontSize: 20.0)),
                  icon: new Icon(Icons.people,size: 50.0,color:Colors.deepPurple)),
              new MyCard(  title: new Text("Mostafiz",style: new TextStyle(fontSize: 20.0)),
                  icon: new Icon(Icons.people,size: 50.0,color:Colors.deepPurple)),
            ],
          ),
        ),

    );
  }
}
class MyCard extends StatelessWidget{

  MyCard({this.title,this.icon});
  final Widget title;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.only(bottom: 20.0),
      child: new Card(
        child: new Column(
          children: <Widget>[
          this.title,
            this.icon
          ],
        ),
      ),
    );
  }

}