import 'package:flutter/material.dart';

 class MyTextInput extends StatefulWidget{
    @override
    MyTextInputState createState() => new MyTextInputState();
}
class MyTextInputState  extends State<MyTextInput> {
  final TextEditingController controller = new TextEditingController();
  String result ="";
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[
              new TextField(decoration: new InputDecoration(
                hintText: "Text Here"
              ),
                onChanged:(String str){
                setState((){
                  result= result + "\n"+ str ;

                });
                controller.text ="";
                },
                controller: controller,
              ),
              new Text(result),

            ],
          ),
        ),
      ),

    );
  }
}



